# Neural network for the MNIST-numbers-dataset

## Requirements
All the needed requirements are stored in the **requirements.txt**

## Data
To train my neural networks I used the MNIST number dataset. Since it's already implemented in PyTorch (torchvision) it's really handy to use.

## Technique

### Fully connected NN
The first method I tried to predict the correct numbers is a **fully connected neural network**. After various tries I decided to use a learning rate of 0.015, because that's where the NN produced the best results.

It consists of 2 hidden layers between the input and the output layer, with each being a simple linear layer. The **activation function** used is the ReLU function and for the output layer I used the activation function log-softmax connected to the **loss function** NLL (negative log likelihood).

To evaluate, whether the network predicts a number correctly or not, there's a method that outputs an image and it's probabilites for each number.

### CNN
A **convolutional neural network** was used as second approach and as I already thought, it has a better prediction and a lower validation loss.

The CNN consists of 2 convolutional layers with each getting max-pooled with a kernel of size 2x2. Each convolutional layer has a kernel of size 5x5.
After that I added 3 linear layers for flattening and also for the 10 needed output features.

The **activation functions** are the ReLU again and as stated in the NN above, there's also a log-softmax activation function connected to the NLL **loss function**.

To display the output there are 2 ways:  
1) Plotting 6 sample numbers with each prediction written directly onto it as title
2) Looking at the confusion matrix at the end of the jupyter notebook

A possible reason, why the confusion matrix sometimes has an accuracy of 1.0 or more is that it possibly detected multiple numbers in one picture. E.g. a 7 is recognized as 1 and 7.



